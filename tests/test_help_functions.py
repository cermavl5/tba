from main import norm_median
import numpy as np


def test_norm_median():
    data = np.array([1, 2, 3])
    h = norm_median(data)
    h2 = norm_median(np.array([-1, 2, 3, 10, 1, 2, 4]))
    assert h == 0.0 and not h2 == 0.0
