import numpy as np
from pathlib import Path
import pandas as pd


def norm_median(data: np.ndarray):
    ndata = (data - data.mean()) / data.std()
    return np.median(ndata)


# Vypočítat průměrný HR jednou za 10 minut
# Pomoci pandas vytahnout jen hodinu z timestamp
# Vytvořit nový sloupec s hodinou a datumem
# Vytvořit skupiny a na nich spočítat průměr
if __name__ == "__main__":
    data_path = Path("data")
    files = data_path.glob("**/heart_rate.csv")
    for file in files:
        pass

    sample_01 = data_path / Path("01/fitbit/heart_rate.csv")
    s01 = pd.read_csv(sample_01)
    print(s01)
